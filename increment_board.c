/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   increment_board.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 11:26:12 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 17:53:06 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int		ft_null_surr_extent(t_board *board, int i, int j)
{
	int z;
	int res;
	int c;

	c = (board->player == '1') ? 2 : 3;
	z = j - 1;
	res = 0;
	while (z < j + 2)
	{
		if ((board->tab[i - 1][z] < 0 && board->tab[i - 1][z] % c == 0))
			res++;
		z++;
	}
	z = j - 1;
	while (z < j + 2)
	{
		if ((board->tab[i + 1][z] < 0 && board->tab[i + 1][z] % c == 0))
			res++;
		z++;
	}
	return (res);
}

static void		ft_null_surrounded(t_board *board)
{
	int i;
	int j;
	int count;
	int count_max;
	int c;

	count_max = 1;
	c = (board->player == '1') ? 2 : 3;
	i = 0;
	while (++i < board->lgn - 1)
	{
		j = 0;
		while (++j < board->col - 1)
		{
			count = 0;
			if ((board->tab[i][j + 1] < 0 && board->tab[i][j + 1] % c == 0))
				count++;
			if ((board->tab[i][j - 1] < 0 && board->tab[i][j - 1] % c == 0))
				count++;
			count = count + ft_null_surr_extent(board, i, j);
			if (board->tab[i][j] >= 0 && count >= ISOLATION_PARAM)
				board->tab[i][j] = 80;
		}
	}
}

static int		ft_is_border(t_board *board, int i, int j)
{
	int count;

	count = 0;
	if (i == 0 || j == 0 || j == board->col - 1 || i == board->lgn - 1 ||
			board->col <= 4 || board->lgn <= 4)
		return (0);
	if ((board->tab[i][j + 1] < 0) || (board->tab[i][j - 1] < 0))
		count++;
	if ((board->tab[i - 1][j] < 0) || (board->tab[i + 1][j] < 0))
		count++;
	if (count >= PROXIMITY_PARAM)
		return (1);
	else
		return (0);
}

static void		ft_increment_board_annex(t_board *board, int i, int j)
{
	int k;
	int l;

	k = -1;
	while (++k < board->lgn)
	{
		l = -1;
		while (++l < board->col)
		{
			if (board->tab[k][l] >= 0)
			{
				board->tab[k][l] += ft_abs(k, i) + ft_abs(l, j);
				if (board->tab[k][l] >= board->max)
					board->max = board->tab[k][l];
			}
		}
	}
}

int				ft_increment_board(t_board *board)
{
	int i;
	int j;
	int c;

	i = -1;
	c = (board->player == '1') ? 3 : 2;
	ft_found_enemy(board);
	while (++i < board->lgn)
	{
		j = -1;
		while (++j < board->col)
		{
			if (board->tab[i][j] < 0 && board->tab[i][j] % c == 0 &&
				!ft_is_border(board, i, j))
				ft_increment_board_annex(board, i, j);
		}
	}
	ft_rebase_tab(board);
	if (board->is_small == 0)
		ft_draw_meridians(board);
	ft_boost_close(board);
	ft_kill_super_close(board);
	if (!(board->lgn <= 3 || board->col <= 3))
		ft_null_surrounded(board);
	return (1);
}
