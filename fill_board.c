/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_board.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:47:34 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 18:50:52 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int	ft_fill_board_2(t_board *board, char c, int i, int j)
{
	if (c == 'O')
		board->tab[i - 1][j - 4] = -2;
	else if (c == 'o')
		board->tab[i - 1][j - 4] = -2;
	else if (c == 'X')
		board->tab[i - 1][j - 4] = -3;
	else if (c == 'x')
		board->tab[i - 1][j - 4] = -3;
	else if (c != 'o' && c != 'O' && c != 'x' && c != 'X' && c != '.')
		return (-1);
	return (1);
}

static void	ft_initialize(char **str, int *i)
{
	*str = NULL;
	*i = 0;
	get_next_line(0, str);
	ft_strdel(str);
}

int			ft_fill_board(t_board *board)
{
	char	*str;
	int		i;
	int		j;

	ft_initialize(&str, &i);
	while (++i < board->lgn + 1)
	{
		get_next_line(0, &str);
		if ((int)ft_strlen(str) != board->col + 4)
		{
			ft_free_board_tab(board);
			return (-1);
		}
		j = 3;
		while (++j < board->col + 4)
		{
			if (ft_fill_board_2(board, str[j], i, j) == -1)
			{
				ft_free_board_tab(board);
				return (-1);
			}
		}
		ft_strdel(&str);
	}
	return (1);
}
