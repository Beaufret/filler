/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 11:17:30 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 18:53:54 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_end(char **str)
{
	if (ft_check_board_init(*str) !=  1 && ft_check_piece_init(*str) == 1)
		ft_strdel(str);
	ft_strdel(str);
}
