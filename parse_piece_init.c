/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_piece_init.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 15:36:03 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 16:57:39 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static	void	ft_trim_bottom_lines(t_piece *piece)
{
	int k;
	int l;

	k = piece->lgn - 1;
	while (k >= 0)
	{
		l = 0;
		while (l < piece->col)
		{
			if (piece->tab[k][l] == 1)
				return ;
			l++;
		}
		piece->lgn = piece->lgn - 1;
		k--;
	}
}

static	void	ft_trim_right_cols(t_piece *piece)
{
	int k;
	int l;

	l = piece->col - 1;
	while (l >= 0)
	{
		k = 0;
		while (k < piece->lgn)
		{
			if (piece->tab[k][l] == 1)
				return ;
			k++;
		}
		piece->col = piece->col - 1;
		l--;
	}
}

void			ft_trim_piece(t_piece *piece)
{
	ft_trim_bottom_lines(piece);
	ft_trim_right_cols(piece);
}

int				ft_check_piece_init(char *str)
{
	int i;

	i = 6;
	if (!str)
		return (-1);
	if (!ft_strnequ("Piece ", str, 6))
		return (-1);
	if (str[i] < '0' || str[i] > '9')
		return (-1);
	while (str[i] && str[i] >= '0' && str[i] <= '9')
		i++;
	if (str[i] == ' ')
		i++;
	else
		return (-1);
	if (str[i] < '0' || str[i] > '9')
		return (-1);
	while (str[i] && str[i] >= '0' && str[i] <= '9')
		i++;
	if (str[i] == ':' && !str[i + 1])
		return (1);
	return (0);
}
