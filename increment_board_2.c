/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   increment_board.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 11:26:12 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 15:29:46 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_rebase_tab(t_board *board)
{
	int i;
	int j;

	i = 0;
	while (i < board->lgn)
	{
		j = 0;
		while (j < board->col)
		{
			if (board->tab[i][j] > 0)
				board->tab[i][j] = (98 * board->tab[i][j] / board->max) + 1;
			j++;
		}
		i++;
	}
}

static void	ft_select_close(t_board *board, int i, int j, int sel)
{
	int k;
	int l;
	int param;

	k = -1;
	while (++k < board->lgn)
	{
		l = -1;
		while (++l < board->col)
		{
			if (board->tab[k][l] >= 0)
			{
				param = (board->is_small == 1) ? 2 : CIRCLE_PARAM;
				if (((ft_abs(k, i) + ft_abs(l, j)) <= param &&
					sel == 1))
					board->tab[k][l] = 0;
				if (((ft_abs(k, i) + ft_abs(l, j)) <= INNER_CIRCLE_PROTECT) &&
					sel == 2 && board->is_small != 1)
					board->tab[k][l] = 70;
			}
		}
	}
}

void		ft_boost_close(t_board *board)
{
	int i;
	int j;
	int c;

	i = -1;
	c = (board->player == '1') ? 3 : 2;
	while (++i < board->lgn)
	{
		j = -1;
		while (++j < board->col)
			if (board->tab[i][j] < 0 && board->tab[i][j] % c == 0)
				ft_select_close(board, i, j, 1);
	}
}

void		ft_kill_super_close(t_board *board)
{
	int i;
	int j;
	int c;

	i = -1;
	c = (board->player == '1') ? 3 : 2;
	while (++i < board->lgn)
	{
		j = -1;
		while (++j < board->col)
			if (board->tab[i][j] < 0 && board->tab[i][j] % c == 0)
				ft_select_close(board, i, j, 2);
	}
}
