/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_prelim.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 15:36:03 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 17:41:17 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_check_prelim(char **str)
{
	if (!*str)
		return (-1);
	if (ft_strnequ("$$$ exec p1 : [", *str, 15) ||
		ft_strnequ("$$$ exec p2 : [", *str, 15))
		return (1);
	ft_strdel(str);
	return (0);
}
