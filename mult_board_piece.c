/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mult_board_piece.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:09:57 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 17:32:44 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int		ft_mult_zoom(t_board *board, t_piece *piece, int i, int j)
{
	int k;
	int l;
	int chek_cost[2];
	int player[2];

	player[0] = (board->player == '1') ? -2 : -3;
	player[1] = (board->player == '1') ? -3 : -2;
	k = -1;
	chek_cost[0] = 0;
	chek_cost[1] = 0;
	while (++k < piece->lgn)
	{
		l = -1;
		while (++l < piece->col)
		{
			if (((i + k) >= board->lgn) || ((j + l) >= board->col))
				return (-1);
			if (piece->tab[k][l] == 1 && (board->tab[i + k][j + l] == player[0]
				|| board->tab[i + k][j + l] == player[1]))
				chek_cost[0] += (board->tab[i + k][j + l] == player[0]) ? 1 : 2;
			if (piece->tab[k][l] == 1 && board->tab[i + k][j + l] >= 0)
				chek_cost[1] += (board->tab[i + k][j + l]);
		}
	}
	return ((chek_cost[0] == 1) ? chek_cost[1] : -1);
}

static void		ft_add_board_piece_extend(t_board *board, t_piece *piece,
		int i, int j)
{
	int c;
	int k;
	int l;

	c = (board->player == '1') ? -2 : -3;
	k = -1;
	while (++k < piece->lgn)
	{
		l = -1;
		while (++l < piece->col)
			if (piece->tab[k][l] == 1 && board->tab[i + k][j + l] >= 0)
				board->tab[i + k][j + l] = -1000 + c;
	}
}

void			ft_add_board_piece(t_board *board, t_piece *piece)
{
	int i;
	int j;
	int c;

	c = (board->player == '1') ? -2 : -3;
	i = -1;
	while (++i < board->lgn)
	{
		j = -1;
		while (++j < board->col)
		{
			if (i == piece->res_x && j == piece->res_y)
			{
				ft_add_board_piece_extend(board, piece, i, j);
			}
		}
	}
}

void			ft_mult_board_piece(t_board *board, t_piece *piece)
{
	int i;
	int j;
	int cost;

	i = -1;
	cost = -1;
	while (++i < board->lgn)
	{
		j = -1;
		while (++j < board->col)
		{
			cost = ft_mult_zoom(board, piece, i, j);
			if (cost != -1 && (piece->cost == -1 || cost < piece->cost))
			{
				piece->res_x = i;
				piece->res_y = j;
				piece->cost = cost;
				cost = -1;
			}
		}
	}
	ft_add_board_piece(board, piece);
	ft_print_board_tab(board);
}
