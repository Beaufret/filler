/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ssfar <ssfar@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 09:32:29 by ssfar             #+#    #+#             */
/*   Updated: 2019/10/07 18:45:38 by ssfar            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# define WINW 2080
# define WINH 1080
#include <SDL.h>
#include <SDL_ttf.h>
#include "libft/libft.h"
#include "libft/printf/includes/ft_printf.h"
#include "libft/get_next_line/includes/get_next_line.h"

typedef struct	s_item
{
	char			**tab;
	uint_fast8_t	width;
	uint_fast8_t	height;
}				t_item;

typedef struct	s_filler
{
	uint_fast8_t	me;
	uint_fast8_t	foe;
	t_item			map;
	t_item			piece;
}				t_filler;

typedef struct	s_tmp
{
	uint_fast8_t	i;
	uint_fast8_t	j;
	uint_fast8_t	matching_star;
	int_fast16_t	score;
}				t_tmp;

typedef	struct	s_dot
{
	uint_fast8_t	y;
	uint_fast8_t	x;
}				t_dot;

typedef	struct	s_player
{
	uint_fast16_t	score;
	SDL_Rect		limit;
	SDL_Rect		block;
	SDL_Rect		name_rect;
	SDL_Rect		score_rect;
	SDL_Color		color;
	SDL_Texture		*name_texture;
	char 			*name;
}				t_player;

typedef struct	s_unit
{
	char			**tab;
	uint_fast8_t	w;
	uint_fast8_t	h;
	SDL_Rect		limit;
	SDL_Rect		block;
}				t_unit;

typedef	struct	s_visu
{
	SDL_Window		*window;
	SDL_Renderer	*renderer;
	SDL_Event		event;
	SDL_bool		close;
	TTF_Font		*font;
	t_unit			map;
	t_unit			piece;
	t_player		p1;
	t_player		p2;
	uint_fast8_t	placed;
}				t_visu;

#endif