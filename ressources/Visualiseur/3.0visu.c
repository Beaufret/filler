/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   3.0visu.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/29 13:55:37 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/29 14:08:03 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_ttf.h>

void	ft_quit(t_visu *visu)
{
	if (SDL_WasInit(SDL_INIT_VIDEO))
	{
		if (TTF_WasInit())
		{
			TTF_CloseFont(visu->font);
			SDL_DestroyTexture(visu->p1.name_texture);
			SDL_DestroyTexture(visu->p2.name_texture);
			TTF_Quit();
			free(visu->p1.name);
			free(visu->p2.name);
			visu->map.tab = ft_free_strtab_offset(visu->map.tab, -4);
			visu->piece.tab = ft_free_strtab(visu->piece.tab);
		}
		SDL_DestroyWindow(visu->window);
		SDL_DestroyRenderer(visu->renderer);
		SDL_Quit();
	}
}

void	ft_exit_fail(char *str, t_visu *visu)
{
	if (str)
		ft_dprintf(2, "[red]ERROR : %s[white]\n", str);
	ft_quit(visu);
	exit(EXIT_FAILURE);
}

void	ft_sdl_exit_fail(char *str, t_visu *visu, const char *(*get_err)(void))
{
	if (str)
		SDL_Log("SDL ERROR : %s > %s", str, get_err());
	ft_quit(visu);
	exit(EXIT_FAILURE);
}

void	ft_init_struct(t_visu *visu)
{
	visu->window = NULL;
	visu->renderer = NULL;
	visu->p1.name = NULL;
	visu->p1.name_texture = NULL;
	visu->p2.name = NULL;
	visu->p2.name_texture = NULL;
	visu->map.tab = NULL;
	visu->piece.tab = NULL;
	visu->font = NULL;
}

char	*ft_cut_playername(char *str)
{
	char			*tmp;
	uint_fast8_t	len;

	tmp = ft_strrchr(str, '/');
	if (tmp && tmp[1])
		str = &(tmp[1]);
	len = ft_strchr_len(str, '.');
	if (len != 0)
		return (ft_strndup(str, len));
	return (ft_strdup(str));
}

void	ft_read_playername(char **p1_name, char **p2_name, t_visu *visu)
{
	char	*line;

	line = NULL;
	while (get_next_line(0, &line) > 0 && *line != '$')
		ft_strdel(&line);
	if (!ft_strstr(line, ".filler"))
	{
		ft_strdel(&line);
		ft_exit_fail("Invalid p1", visu);
	}
	*p1_name = ft_cut_playername(line);
	ft_strdel(&line);
	if (!*p1_name)
		ft_exit_fail("Malloc failled : p1 name", visu);
	while (get_next_line(0, &line) > 0 && *line != '$')
		free(line);
	if (!ft_strstr(line, ".filler"))
	{
		ft_strdel(&line);
		ft_exit_fail("Invalid p2", visu);
	}
	*p2_name = ft_cut_playername(line);
	free(line);
	if (!*p2_name)
		ft_exit_fail("Malloc failled : p2 name", visu);
}

void	ft_format_text(t_player *p, SDL_Renderer *renderer, t_visu *visu)
{
	SDL_Surface	*tmp_surface;

	p->color.r = 0;
	p->color.a = 255;
	p->name_rect.y = 60;
	p->name_rect.w = 15 * ft_strlen(p->name);
	p->name_rect.h = 40;
	p->score = 0;
	p->score_rect.x = p->name_rect.x + p->name_rect.w + 10;
	p->score_rect.y = 60;
	p->score_rect.h = p->name_rect.h;
	if (!(tmp_surface = TTF_RenderText_Blended(visu->font, p->name, p->color)))
		ft_sdl_exit_fail("RenderText() of p name failed", visu, &TTF_GetError);
	p->name_texture = SDL_CreateTextureFromSurface(renderer, tmp_surface);
	SDL_FreeSurface(tmp_surface);
	if (!p->name_texture)
		ft_sdl_exit_fail("TextureFromSurface() failed", visu, &SDL_GetError);
}

void	ft_init_text(t_player *p1, t_player *p2, SDL_Renderer *renderer,
	t_visu *visu)
{
	p1->color.g = 0;
	p1->color.b = 255;
	p2->color.g = 255;
	p2->color.b = 0;
	p1->name_rect.x = 40;
	p2->name_rect.x = 1560;
	ft_format_text(p1, renderer, visu);
	ft_format_text(p2, renderer, visu);
}

void	ft_get_unit(t_unit *item, char *line, uint_fast8_t jump)
{
	uint_fast8_t	i;

	item->h = ft_atoi(line);
	item->w = ft_atoi(ft_strrchr(line, ' '));
	i = 0;
	if ((item->tab = malloc(sizeof(*(item->tab)) * (item->h + 1))))
	{
		if (jump && get_next_line(0, &line) > 0)
			free(line);
		while (i < item->h && get_next_line(0, &line) > 0)
			item->tab[i++] = line + jump;
		item->tab[i] = NULL;
	}
}

uint_fast8_t	ft_read_round(t_unit *map, t_unit *piece)
{
	char	*line;

	line = NULL;
	while (get_next_line(0, &line) > 0 && *line != '=')
	{
		if (line)
		{
			if (ft_strncmp(line, "Plateau", 2) == 0)
			{
				map->tab = ft_free_strtab_offset(map->tab, -4);
				ft_get_unit(map, ft_strchr(line, ' '), 4);
			}
			else if (ft_strncmp(line, "Piece", 2) == 0)
			{
				piece->tab = ft_free_strtab(piece->tab);
				ft_get_unit(piece, ft_strchr(line, ' '), 0);
				ft_strdel(&line);
				return (1);
			}
			ft_strdel(&line);
		}
	}
	ft_strdel(&line);
	return (0);
}

void	ft_set_block_color(char c, t_visu *visu)
{
	if (c == 'O')
	{
		if (SDL_SetRenderDrawColor(visu->renderer, 0, 0, 190, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for map.block failed", visu, &SDL_GetError);
	}
	else if (c == 'o')
	{
		if (SDL_SetRenderDrawColor(visu->renderer, 0, 70, 255, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for map.block failed", visu, &SDL_GetError);
	}
	else if (c == 'X')
	{
		if (SDL_SetRenderDrawColor(visu->renderer, 0, 190, 0, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for map.block failed", visu, &SDL_GetError);
	}
	else if (c == 'x')
	{
		if (SDL_SetRenderDrawColor(visu->renderer, 0, 255, 70, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for map.block failed", visu, &SDL_GetError);
	}
	else
		if (SDL_SetRenderDrawColor(visu->renderer, 30, 30, 30, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for map.block failed", visu, &SDL_GetError);
}

void	ft_load_map(t_visu *visu)
{
	uint_fast8_t	i;
	uint_fast8_t	j;

	if (SDL_SetRenderDrawColor(visu->renderer, 255, 0, 0, 255) < 0)
		ft_sdl_exit_fail("SetRenderDrawColor() for map.limit failed", visu, &SDL_GetError);
	else if (SDL_RenderDrawRect(visu->renderer, &visu->map.limit) < 0)
		ft_sdl_exit_fail("RenderDrawRect() of map.limit failed", visu, &SDL_GetError);
	visu->map.block.y = visu->map.limit.y + 1;
	i = 0;
	while (visu->map.tab[i])
	{
		visu->map.block.x = visu->map.limit.x + 1;
		j = 0;
		while (visu->map.tab[i][j])
		{
			ft_set_block_color(visu->map.tab[i][j++], visu);
			if (SDL_RenderFillRect(visu->renderer, &visu->map.block) < 0)
				ft_sdl_exit_fail("RenderDrawRect() of map.block failed", visu, &SDL_GetError);
			visu->map.block.x += visu->map.block.w + 1;
		}
		i++;
		visu->map.block.y += visu->map.block.h + 1;
	}
}

void	ft_load_score(t_player *p, TTF_Font *font, SDL_Renderer *renderer, t_visu *visu)
{
	SDL_Surface		*tmp_surface;
	SDL_Texture		*tmp_texture;
	char			*score_str;

	p->score_rect.w = 15 * ft_uintlen(p->score);
	if (!(score_str = ft_utoa(p->score)))
		ft_exit_fail("Malloc failled : utoa score", visu);
	tmp_surface = TTF_RenderText_Blended(font, score_str, p->color);
	free(score_str);
	if (!tmp_surface)
		ft_sdl_exit_fail("RenderText() score", visu, &TTF_GetError);
	tmp_texture = SDL_CreateTextureFromSurface(renderer, tmp_surface);
	SDL_FreeSurface(tmp_surface);
	if (!tmp_texture)
		ft_sdl_exit_fail("TextureFromSurface() score", visu, &SDL_GetError);
	if (SDL_RenderCopy(renderer, tmp_texture, NULL, &(p->score_rect)) < 0)
	{
		SDL_DestroyTexture(tmp_texture);
		ft_sdl_exit_fail("RenderCopy() score", visu, &SDL_GetError);
	}
	SDL_DestroyTexture(tmp_texture);
	if (SDL_RenderCopy(renderer, p->name_texture, NULL, &p->name_rect) < 0)
		ft_sdl_exit_fail("RenderCopy() of p name failed", visu, &SDL_GetError);
}

void	ft_init_map(t_unit	*map)
{
	map->block.w = 900 / map->w;
	map->block.h = 900 / map->h;
	map->limit.w = (map->block.w * map->w) + map->w + 1;
	map->limit.h = (map->block.h * map->h) + map->h + 1;
	map->limit.x = (WINW - map->limit.w) / 2;
	map->limit.y = (WINH - map->limit.h) / 2;
}

void	ft_increment_score(char c, t_visu *visu)
{
	if (c == 'o')
	{
		if (visu->placed == 0 || visu->placed == 2)
		{
			visu->p1.score++;
			visu->placed = 1;
		}
		else if (visu->placed == 1)
			visu->placed = 3;
		else
			visu->p1.score++;
	}
	else if (c == 'x')
	{
		if (visu->placed == 0 || visu->placed == 2)
			visu->placed = 4;
		else if (visu->placed == 1)
		{
			visu->p2.score++;
			visu->placed = 2;
		}
		else
			visu->p2.score++;
	}
}

void	ft_set_piece_color(t_visu *visu, uint_fast8_t color, uint_fast8_t star)
{
	if (!star)
	{
		if (SDL_SetRenderDrawColor(visu->renderer, 30, 30, 30, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for piece.block failed", visu, &SDL_GetError);
	}
	else if (color == 1)
	{
		//if (SDL_SetRenderDrawColor(visu->renderer, 0, 0, 255, 255) < 0)
		if (SDL_SetRenderDrawColor(visu->renderer, 0, 0, 190, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for piece.block failed", visu, &SDL_GetError);
	}
	else
		//if (SDL_SetRenderDrawColor(visu->renderer, 0, 255, 0, 255) < 0)
		if (SDL_SetRenderDrawColor(visu->renderer, 0, 190, 0, 255) < 0)
			ft_sdl_exit_fail("SetRenderDrawColor() for piece.block failed", visu, &SDL_GetError);
}

void	ft_load_piece(t_visu *visu, uint_fast8_t color)
{
	uint_fast8_t	i;
	uint_fast8_t	j;

	i = 0;
	while (visu->piece.tab[i])
	{
		visu->piece.block.x = visu->piece.limit.x + 1;
		j = 0;
		while (visu->piece.tab[i][j])
		{
			if (visu->piece.tab[i][j++] == '*')
				ft_set_piece_color(visu, color, 1);
			else
				ft_set_piece_color(visu, color, 0);
			if (SDL_RenderFillRect(visu->renderer, &visu->piece.block) < 0)
				ft_sdl_exit_fail("RenderDrawRect() of map.block failed", visu, &SDL_GetError);
			visu->piece.block.x += visu->piece.block.w + 1;	
		}
		visu->piece.block.y += visu->piece.block.h + 1;
		i++;
	}
}

void	ft_load_side(t_visu *visu)
{
	uint_fast8_t color;

	if (SDL_SetRenderDrawColor(visu->renderer, 255, 255, 255, 255) < 0)
		ft_sdl_exit_fail("SetRenderDrawColor() for piece.limit failed", visu, &SDL_GetError);
	visu->piece.block.w = 1.5 * visu->map.block.w;
	visu->piece.block.h = 1.5 * visu->map.block.h;
	visu->piece.limit.w = (visu->piece.block.w * visu->piece.w) + visu->piece.w + 1;
	visu->piece.limit.h = (visu->piece.block.h * visu->piece.h) + visu->piece.h + 1;
	visu->piece.limit.y = visu->p1.name_rect.y + visu->p1.name_rect.h + 150;
	visu->piece.block.y = visu->piece.limit.y + 1;
	if (visu->placed == 2 || visu->placed == 3 || visu->placed == 0)
	{
		visu->piece.limit.x = visu->p1.name_rect.x;
		color = 1;
	}
	else
	{
		visu->piece.limit.x = visu->p2.name_rect.x;
		color = 2;
	}
	if (SDL_RenderDrawRect(visu->renderer, &visu->piece.limit) < 0)
		ft_sdl_exit_fail("RenderDrawRect() of piece.limit failed", visu, &SDL_GetError);
	ft_load_piece(visu, color);
}

void	ft_get_active_player(t_visu *visu)
{
	uint_fast8_t	i;
	uint_fast8_t	j;

	i = 0;
	while (visu->map.tab[i])
	{
		j = 0;
		while (visu->map.tab[i][j])
		{
			if ((visu->map.tab[i][j]) == 'o' || (visu->map.tab[i][j]) == 'x')
				return (ft_increment_score(visu->map.tab[i][j], visu));
			j++;
		}
		i++;
	}
}

void	ft_init_first_round(t_visu *visu)
{
	ft_read_playername(&visu->p1.name, &visu->p2.name, visu);
	ft_init_text(&visu->p1, &visu->p2, visu->renderer, visu);
	if (!ft_read_round(&visu->map, &visu->piece) || !visu->map.tab
		|| !visu->piece.tab)
		ft_exit_fail("No round to read OR malloc error", visu);
	ft_init_map(&visu->map);
	visu->placed = 0;
	ft_load_map(visu);
	ft_get_active_player(visu);
	ft_load_side(visu);
	ft_load_score(&visu->p1, visu->font, visu->renderer, visu);
	ft_load_score(&visu->p2, visu->font, visu->renderer, visu);
}

void	ft_init(t_visu *visu)
{
	ft_init_struct(visu);
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		ft_sdl_exit_fail("Init() of video failed", visu, &SDL_GetError);
	if (!(visu->window = SDL_CreateWindow("Filler Visualiseur",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINW, WINH, 0)))
		ft_sdl_exit_fail("CreateWindow() failed", visu, &SDL_GetError);
	if (!(visu->renderer = SDL_CreateRenderer(visu->window, -1,
		SDL_RENDERER_SOFTWARE)))
		ft_sdl_exit_fail("CreateRenderer() failed", visu, &SDL_GetError);
	if (TTF_Init() < 0)
		ft_sdl_exit_fail("Init() failed", visu, &TTF_GetError);
	if (!(visu->font = TTF_OpenFont("/Library/Fonts/Arial.ttf", 30)))
		ft_sdl_exit_fail("OpenFont() of Arial failed", visu, &TTF_GetError);
	ft_init_first_round(visu);
	SDL_RenderPresent(visu->renderer);
}

uint_fast8_t	ft_display_round(t_visu *visu)
{
	if (SDL_SetRenderDrawColor(visu->renderer, 0,0,0,255) < 0)
		ft_sdl_exit_fail("SetRenderDrawColor() for RenderClear failed", visu, &SDL_GetError);
	if (SDL_RenderClear(visu->renderer) < 0)
		ft_sdl_exit_fail("RenderClear() failed", visu, &SDL_GetError);
	if (!ft_read_round(&visu->map, &visu->piece) || !visu->map.tab
		|| !visu->piece.tab)
		return (0);
	ft_load_map(visu);
	ft_get_active_player(visu);
	ft_load_side(visu);
	ft_load_score(&visu->p1, visu->font, visu->renderer, visu);
	ft_load_score(&visu->p2, visu->font, visu->renderer, visu);
	SDL_RenderPresent(visu->renderer);
	return (1);
}

void	ft_visu(t_visu *visu)
{
	SDL_bool	playing;

	playing = SDL_TRUE;
	visu->close = SDL_FALSE;
	while (!visu->close && SDL_WaitEvent(&visu->event))
	{
		if (visu->event.type == SDL_QUIT)
			visu->close = SDL_TRUE;
		else if (visu->event.type == SDL_KEYDOWN)
		{
			if (visu->event.key.keysym.sym == SDLK_ESCAPE)
				visu->close = SDL_TRUE;
			else if (playing && visu->event.key.keysym.sym == SDLK_RETURN)
				if (!ft_display_round(visu))
					playing = SDL_FALSE;
		}
	}
}

int	main(int argc, char *argv[])
{
	t_visu	visu;

	ft_init(&visu);
	ft_visu(&visu);
	ft_quit(&visu);
}

/*
	printf("window %d, renderer %d font %d\n", visu->window, visu->renderer, visu->font);
	printf("p1 name %s\n", visu->p1.name);
	printf("p2 name %s\n", visu->p2.name);
	printf("map w %d h %d\n", visu->map.w, visu->map.h);
	printf("piece w %d h %d\n", visu->piece.w, visu->piece.h);
	printf("p1 color r%d g%d b%d a%d\n", visu->p1.color.r, visu->p1.color.g, visu->p1.color.b, visu->p1.color.a);
	printf("p2 color r%d g%d b%d a%d\n", visu->p2.color.r, visu->p2.color.g, visu->p2.color.b, visu->p2.color.a);
	printf("p1 name_rect w%d h%d x%d y%d\n", visu->p1.name_rect.w, visu->p1.name_rect.h, visu->p1.name_rect.x, visu->p1.name_rect.y);
	printf("p2 name_rect w%d h%d x%d y%d\n", visu->p2.name_rect.w, visu->p2.name_rect.h, visu->p2.name_rect.x, visu->p2.name_rect.y);
	printf("map : %s\n", visu->map.tab[0]);
	printf("piece : %s\n", visu->piece.tab[0]);
	printf("map limit w %d h %d x %d y %d\n", visu->map.limit.w, visu->map.limit.h, visu->map.limit.x, visu->map.limit.y);
	printf("map block w %d h %d x %d y %d\n", visu->map.block.w, visu->map.block.h, visu->map.block.x, visu->map.block.y);
*/
