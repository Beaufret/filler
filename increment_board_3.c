/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   increment_board_3.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 11:26:12 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 15:13:16 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_found_enemy(t_board *board)
{
	int i;
	int j;
	int c;

	c = (board->player == '1') ? 3 : 2;
	if (board->lgn <= 3 || board->col <= 3)
		return ;
	i = 0;
	while (++i < board->lgn - 1)
	{
		j = 0;
		while (++j < board->col - 1)
		{
			if ((board->tab[i][j] < 0 && board->tab[i][j + 1] % c != 0) &&
				((board->tab[i][j + 1] < 0 && board->tab[i][j + 1] % c == 0) ||
				(board->tab[i][j - 1] < 0 && board->tab[i][j - 1] % c == 0) ||
				(board->tab[i - 1][j] < 0 && board->tab[i - 1][j] % c == 0) ||
				(board->tab[i + 1][j] < 0 && board->tab[i + 1][j] % c == 0)))
			{
				board->found_enemy = 1;
				return ;
			}
		}
	}
}
