/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/17 07:39:34 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 18:51:45 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# define THRESHOLD (80)
# define CIRCLE_PARAM (6)
# define INNER_CIRCLE_PROTECT (CIRCLE_PARAM - 2)
# define PROXIMITY_PARAM (2)
# define ISOLATION_PARAM (4)

# include <stdlib.h>
# include <unistd.h>
# include "libft/libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

typedef struct s_fd_list	t_fd_list;
typedef struct s_board		t_board;
typedef struct s_piece		t_piece;

struct	s_fd_list
{
	char		*str;
	int			fd;
	t_fd_list	*next;
};

struct	s_board
{
	int			col;
	int			lgn;
	char		player;
	int			**tab;
	int			max;
	int			found_enemy;
	int			is_small;
};

struct	s_piece
{
	int			col;
	int			lgn;
	int			**tab;
	int			res_x;
	int			res_y;
	int			cost;
};

int		get_next_line(const int fd, char **line);
int		ft_check_prelim(char **str);
void	ft_free_board_tab(t_board *board);
void	ft_print_board_tab(t_board *board);
void	ft_init_board_tab(t_board *board);
void	ft_create_board_tab(t_board *board);
void	ft_init_board(t_board *board);
void	ft_free_piece_tab(t_piece *piece);
void	ft_print_piece_tab(t_piece *piece);
void	ft_init_piece_tab(t_piece *piece);
void	ft_create_piece_tab(t_piece *piece);
void	ft_init_piece(t_piece *piece);
void	ft_init_piece(t_piece *piece);
int		ft_fill_board(t_board *board);
int		ft_fill_piece(t_board *board, t_piece *piece);
int		ft_increment_board(t_board *board);
int		ft_check_board_init(char *str);
int		ft_check_piece_init(char *str);
void	ft_mult_board_piece(t_board *board, t_piece *piece);
void	ft_add_board_piece(t_board *board, t_piece *piece);
void	ft_trim_piece(t_piece *piece);
void	ft_draw_meridians(t_board *board);
void	ft_kill_super_close(t_board *board);
void	ft_boost_close(t_board *board);
void	ft_rebase_tab(t_board *board);
void	ft_found_enemy(t_board *board);
void	ft_end(char **str);

#endif
