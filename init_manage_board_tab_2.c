/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_manage_board_tab_2.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 16:32:50 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 17:32:48 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void			ft_free_board_tab(t_board *board)
{
	int i;

	i = 0;
	while (i < board->lgn && board->tab[i])
	{
		free(board->tab[i]);
		board->tab[i] = NULL;
		i++;
	}
	free(board->tab);
}
