/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 11:17:30 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 18:49:24 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int		ft_initialize(t_board *board, t_piece *piece, char **str)
{
	*str = NULL;
	ft_init_board(board);
	ft_init_piece(piece);
	get_next_line(0, str);
	if (ft_check_prelim(str) != 1)
		return (-1);
	board->player = (*str)[10];
	ft_strdel(str);
	return (1);
}

static void		ft_first_if(t_board *board, char **str)
{
	board->lgn = ft_atoi(*str + 8);
	board->col = ft_atoi(*str + 8 + ft_sizeof_nbr(board->lgn));
	ft_strdel(str);
	ft_create_board_tab(board);
	ft_init_board_tab(board);
}

static void		ft_second_if(t_piece *piece, char **str)
{
	piece->lgn = ft_atoi(*str + 6);
	piece->col = ft_atoi(*str + 6 + ft_sizeof_nbr(piece->lgn));
	ft_strdel(str);
	ft_create_piece_tab(piece);
	ft_init_piece_tab(piece);
}

static void		ft_third_part(t_board *board, t_piece *piece)
{
	ft_trim_piece(piece);
	ft_mult_board_piece(board, piece);
	ft_printf_err("{GREEN} Output [%i, %i]{EOC}\n", piece->res_x, piece->res_y);
	ft_printf("%i %i\n", piece->res_x, piece->res_y);
	//ft_init_piece(piece);
	ft_init_board_tab(board);
	ft_init_piece_tab(piece);
	ft_free_board_tab(board);
	ft_free_piece_tab(piece);
}

int				main(void)
{
	t_board	board;
	t_piece	piece;
	char	*str;

	if (ft_initialize(&board, &piece, &str) != 1)
		return (0);
	while (get_next_line(0, &str))
	{
		if (ft_check_board_init(str) == 1)
		{
			ft_first_if(&board, &str);
			if (ft_fill_board(&board) < 0 || ft_increment_board(&board) == -1)
				return (0);
			get_next_line(0, &str);
		}
		if (ft_check_piece_init(str) == 1)
		{
			ft_second_if(&piece, &str);
			if (ft_fill_piece(&board, &piece) == -1)
				return (0);
			ft_third_part(&board, &piece);
		}
		ft_end(&str);
	}
	return (0);
}
