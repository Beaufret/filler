/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_board_init.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 15:36:03 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 17:49:57 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_check_board_init(char *str)
{
	int i;

	i = 8;
	if (!str)
		return (-1);
	if (!ft_strnequ("Plateau ", str, 8))
		return (-1);
	if (str[i] < '0' || str[i] > '9')
		return (-1);
	while (str[i] && str[i] >= '0' && str[i] <= '9')
		i++;
	if (str[i] == ' ')
		i++;
	if (str[i] < '0' || str[i] > '9')
		return (-1);
	while (str[i] && str[i] >= '0' && str[i] <= '9')
		i++;
	if (str[i] == ':' && !str[i + 1])
		return (1);
	return (0);
}
