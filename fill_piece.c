/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_piece.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:47:34 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 18:01:04 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int			ft_fill_piece_2(t_piece *piece, char c, int i, int j)
{
	if (c == '.')
		piece->tab[i][j] = 0;
	else if (c == '*')
		piece->tab[i][j] = 1;
	else if (c != '.' && c != '*')
		return (-1);
	return (1);
}

static void			ft_free_piece_and_board(t_board *board, t_piece *piece)
{
	ft_free_piece_tab(piece);
	ft_free_board_tab(board);
}

static void			ft_initialize(char **str, int *i)
{
	*str = NULL;
	*i = -1;
}

int					ft_fill_piece(t_board *board, t_piece *piece)
{
	char	*str;
	int		i;
	int		j;

	ft_initialize(&str, &i);
	while (++i < piece->lgn)
	{
		get_next_line(0, &str);
		if ((int)ft_strlen(str) != piece->col)
		{
			ft_free_piece_and_board(board, piece);
			return (-1);
		}
		j = -1;
		while (++j < piece->col)
		{
			if (ft_fill_piece_2(piece, str[j], i, j) == -1)
			{
				ft_free_piece_and_board(board, piece);
				return (-1);
			}
		}
		ft_strdel(&str);
	}
	return (1);
}
