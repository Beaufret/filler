/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_manage_piece_tab.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 16:32:50 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 16:19:21 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void			ft_free_piece_tab(t_piece *piece)
{
	int i;

	i = 0;
	while (i < piece->lgn && piece->tab[i])
	{
		free(piece->tab[i]);
		piece->tab[i] = NULL;
		i++;
	}
	free(piece->tab);
}

static void		ft_print_piece_special_char(t_piece *piece, int i, int j)
{
	if (piece->tab[i][j] == 0)
		ft_printf("%c", '.');
	else if (piece->tab[i][j] == 1)
		ft_printf("{YELLOW}%s{EOC}", "*");
}

void			ft_print_piece_tab(t_piece *piece)
{
	int i;
	int j;

	i = 0;
	while (i < piece->lgn)
	{
		j = 0;
		while (j < piece->col)
		{
			ft_print_piece_special_char(piece, i, j);
			if (j != piece->col - 1)
				ft_printf("-");
			j++;
		}
		ft_printf("\n");
		i++;
	}
}

void			ft_init_piece_tab(t_piece *piece)
{
	int i;
	int j;

	i = 0;
	while (i < piece->lgn)
	{
		j = 0;
		while (j < piece->col)
		{
			piece->tab[i][j] = 0;
			j++;
		}
		i++;
	}
}

void			ft_create_piece_tab(t_piece *piece)
{
	int i;

	if (!(piece->tab = (int **)malloc(sizeof(int *) * piece->lgn)))
	{
		ft_free_piece_tab(piece);
		return ;
	}
	i = 0;
	while (i < piece->lgn)
	{
		piece->tab[i] = NULL;
		if (!(piece->tab[i] = (int *)malloc(sizeof(int) * piece->col)))
		{
			ft_free_piece_tab(piece);
			return ;
		}
		i++;
	}
}
