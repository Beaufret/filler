/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   increment_board_cardinals.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 11:26:12 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/07 21:51:04 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_rush_south(t_board *board)
{
	int i;
	int j;
	int c;
	int k;

	i = 0;
	c = (board->player == '1') ? 2 : 3;

	i = 0;
	while (i < board->lgn - 1)
	{
		j = 0;
		while (j < board->col - 1)
		{

			if (board->tab[i][j] < 0 && board->tab[i][j] % c == 0 && i + 1 <= board->lgn)
			{
				k = i + 1;
				while (k < board->lgn && board->tab[k][j] >= 0)
					k++;
				if (k == board->lgn)
				{
					k = i + 1;
					while (k < board->lgn)
					{
						board->tab[k][j] = 0;
						k++;
					}
				}
			}
			j++;
		}
		i++;
	}
	return ;
}
