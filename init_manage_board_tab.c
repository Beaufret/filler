/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_manage_board_tab.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 16:32:50 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/30 17:36:34 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void		ft_print_board_annex(t_board *board, int i, int j)
{
	if (board->tab[i][j] < 10)
	{
		ft_printf_err("{MAGENTA}%s{EOC}", "0");
		ft_printf_err("{MAGENTA}%i{EOC}", board->tab[i][j]);
	}
	else
		ft_printf_err("%i", board->tab[i][j]);
}

static void		ft_print_board_special_char(t_board *board, int i, int j)
{
	if (board->tab[i][j] == 0)
		ft_printf_err("{YELLOW}%s{EOC}", "00");
	else if (board->tab[i][j] == -2)
	{
		if (board->player == '2')
			ft_printf_err("{RED}%s{EOC}", "!O");
		else
			ft_printf_err("{GREEN}%s{EOC}", "!O");
	}
	else if (board->tab[i][j] == -3)
	{
		if (board->player == '2')
			ft_printf_err("{GREEN}%s{EOC}", "!X");
		else
			ft_printf_err("{RED}%s{EOC}", "!X");
	}
	else if (board->tab[i][j] == -1002)
		ft_printf_err("{YELLOW}%s{EOC}", "!o");
	else if (board->tab[i][j] == -1003)
		ft_printf_err("{YELLOW}%s{EOC}", "!x");
	else
		ft_print_board_annex(board, i, j);
}

void			ft_print_board_tab(t_board *board)
{
	int i;
	int j;

	i = 0;
	while (i < board->lgn)
	{
		j = 0;
		while (j < board->col)
		{
			ft_print_board_special_char(board, i, j);
			if (j != board->col - 1)
				ft_printf_err("-");
			j++;
		}
		ft_printf_err("\n");
		i++;
	}
}

void			ft_init_board_tab(t_board *board)
{
	int i;
	int j;

	i = 0;
	while (i < board->lgn)
	{
		j = 0;
		while (j < board->col)
		{
			board->tab[i][j] = 0;
			j++;
		}
		i++;
	}
}

void			ft_create_board_tab(t_board *board)
{
	int i;

	board->is_small = (board->lgn <= 30 || board->col <= 30) ? 1 : 0;
	if (!(board->tab = (int **)malloc(sizeof(int *) * board->lgn)))
	{
		ft_free_board_tab(board);
		return ;
	}
	i = 0;
	while (i < board->lgn)
	{
		board->tab[i] = NULL;
		if (!(board->tab[i] = (int *)malloc(sizeof(int) * board->col)))
		{
			ft_free_board_tab(board);
			return ;
		}
		i++;
	}
}
