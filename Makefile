# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/06/03 14:03:16 by rbeaufre          #+#    #+#              #
#    Updated: 2019/10/30 18:51:20 by rbeaufre         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = rbeaufre.filler

MAKEFILE = Makefile
BIN_DIR = .
SRC_DIR = .
OBJ_DIR = obj
LIBFT_DIR = libft
LIBFT = $(LIBFT_DIR)/libft.a

SRC_RAW = get_next_line.c filler.c parse_prelim.c init_manage_board_tab.c \
			init_board_and_tab.c fill_board.c fill_piece.c increment_board.c \
			parse_board_init.c parse_piece_init.c init_manage_piece_tab.c \
			mult_board_piece.c increment_board_3.c init_manage_board_tab_2.c \
			increment_meridians.c increment_board_2.c filler_2.c \

SRC = $(addprefix $(SRC_DIR)/, $(SRC_RAW))

OBJ = $(addprefix $(OBJ_DIR)/, $(subst .c,.o,$(SRC_RAW)))

HEADERS = filler.h

CCC = gcc
CFLAGS = -O3 -Wall -Wextra -Werror -I$(LIBFT_DIR)

GREEN := "\033[0;32m"
CYAN := "\033[0;36m"
RESET :="\033[0m"

all:
		@(cd $(LIBFT_DIR) && $(MAKE))
		@mkdir -p $(OBJ_DIR)
		@make $(BIN_DIR)/$(NAME)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(HEADERS) $(MAKEFILE)
		@$(CCC) -c $< -o $@ $(CFLAGS)

$(BIN_DIR)/$(NAME): $(OBJ_DIR) $(OBJ) $(LIBFT) 
		@$(CCC) $(CFLAGS) $(OBJ) $(LIBFT) -o $(NAME)
		@echo ${GREEN}"Compiled $(NAME) with success"${RESET}

clean:
		@rm -f $(OBJ)
		@rm -Rf $(OBJ_DIR)
		@(cd $(LIBFT_DIR) && $(MAKE) $@)
		@echo ${CYAN}"Cleaned $(NAME) objects with success"${RESET}

fclean: clean
		@rm -f $(NAME)
		@(cd $(LIBFT_DIR) && $(MAKE) $@)
		@echo ${CYAN}"Removed $(BIN_DIR)/$(NAME) with success"${RESET}

re: fclean all

.PHONY: clean fclean
