/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   increment_board_cardinals.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 11:26:12 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/29 18:13:33 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void		ft_setting_cols_to_zero(t_board *board,
		int staying_value, int from, int to)
{
	int u;

	u = from;
	if (board->col <= 5 || board->lgn <= 5)
		return ;
	while (u < to)
	{
		if (board->tab[staying_value][u] >= 0)
			board->tab[staying_value][u] = 0;
		u++;
	}
}

static void		ft_setting_lgns_to_zero(t_board *board,
		int staying_value, int from, int to)
{
	int u;

	u = from;
	if (board->col <= 5 || board->lgn <= 5)
		return ;
	while (u < to)
	{
		if (board->tab[u][staying_value] >= 0)
			board->tab[u][staying_value] = 0;
		u++;
	}
}

static void		ft_set(t_board *board, int i, int j)
{
	if (board->tab[i][j] >= 0)
		board->tab[i][j] = 0;
	if (board->tab[i][j + 1] >= 0)
		board->tab[i][j + 1] = 0;
}

static void		ft_draw_meridians_2(t_board *board)
{
	int j;
	int i;

	if (board->found_enemy == 1 || board->lgn <= 10 || board->col <= 10)
		return ;

	i = board->lgn / 2;
	j = 0;
	while (i < board->lgn - 1 && j < board->col / 4)
		ft_set(board, i++, j++);
	j = 3 * board->col / 4;
	while (i > board->lgn / 2 && j < board->col - 2)
		ft_set(board, i--, j++);
	while (i > board->lgn / 4 && j > board->col / 2)	
		ft_set(board, i--, j--);
	j = board->col / 4;
	while (i < board->lgn - 2 && j > 0)	
		ft_set(board, i++, j--);

}

void			ft_draw_meridians(t_board *board)
{
	if (board->found_enemy == 1 || board->lgn <= 10 || board->col <= 10)
		return ;
	ft_setting_cols_to_zero(board, 0, 0, board->col - 1);
	ft_setting_cols_to_zero(board, board->lgn - 1, 0, board->col);
	ft_setting_cols_to_zero(board, 3 * board->lgn / 4, board->col / 4, 3 * board->col / 4);
	ft_setting_cols_to_zero(board, board->lgn / 4, board->col / 4, 3 * board->col / 4);

	ft_setting_cols_to_zero(board, 3 * board->lgn / 4 - 1, board->col / 4, 3 * board->col / 4);
	ft_setting_cols_to_zero(board, board->lgn / 4 - 1, board->col / 4, 3 * board->col / 4);

	ft_setting_lgns_to_zero(board, board->col - 1, 0, board->lgn);
	ft_setting_lgns_to_zero(board, 0, 0, board->lgn - 1);
	//ft_setting_lgns_to_zero(board, board->col / 4 - 1, 0, 3 * board->lgn / 4);
	//ft_setting_lgns_to_zero(board, board->col / 4, 0, 3 * board->lgn / 4);
	//ft_setting_lgns_to_zero(board, 3 * board->col / 4 - 1, 0, board->lgn);
	//ft_setting_lgns_to_zero(board, 3 * board->col / 4, 0, board->lgn);
	ft_draw_meridians_2(board);
	return ;
}
