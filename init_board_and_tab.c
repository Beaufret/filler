/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_board_and_tab.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:09:57 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/10/29 16:43:19 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_init_board(t_board *board)
{
	board->col = 0;
	board->lgn = 0;
	board->player = '1';
	board->max = 4;
	board->found_enemy = 0;
	board->is_small = 0;
}

void	ft_init_piece(t_piece *piece)
{
	piece->col = 0;
	piece->lgn = 0;
	piece->res_x = -1;
	piece->res_y = -1;
	piece->cost = -1;
}
